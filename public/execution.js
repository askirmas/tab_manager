chrome.browserAction.onClicked.addListener(() => activateBackPage());

function activateBackPage() {
  const appUrl = chrome.extension.getURL('index.html'); 
  chrome.tabs.query(
    {currentWindow: true, url: appUrl},
    tabs => {
      if (tabs.length === 0)
        chrome.tabs.create({url: appUrl})
      else
        chrome.tabs.update(tabs[0].id, {active: true})
    }
  );
}
