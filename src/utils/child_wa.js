export function removeChild(target, id2remove) {
  assignChildren(target,
    target.state.childrenIds.filter(id => id !== id2remove)
  )
}

export function addChild(target, id2add) {
  assignChildren(target, target.state.childrenIds.concat([id2add]))    
}

export function assignChildren(target, childrenIds) {
  target.setState({childrenIds})
}
