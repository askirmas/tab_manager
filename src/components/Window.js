/*global chrome*/
import React from 'react'
import PureChildrenedComponent from './basic/PureChildrenedComponent'
import './Window.css'

import Tab from './Tab'

export default class Window extends PureChildrenedComponent {
  constructor(props) {
    super(props)
    console.log(props.viewStyle)

    const {id} = props

    /*
      (optional) https://developer.chrome.com/extensions/tabs#type-Tab
      Under some circumstances a Tab may not be assigned an ID, for example when querying foreign tabs using the sessions API, in which case a session ID may be present. Tab ID can also be set to chrome.tabs.TAB_ID_NONE for apps and devtools windows. 
    */
    chrome.tabs.query(
      {windowId: id},
      tabs => this.setChildren(tabs.map(t => t.id))
    )

    const adding = (windowId, tabId) => {
      if (id === windowId)
        this.addChild(tabId)
    }
    const attaching = (tabId, {newWindowId}) => adding(newWindowId, tabId),
      creating = tab => adding(tab.windowId, tab.id)
    chrome.tabs.onAttached.addListener(attaching)
    chrome.tabs.onCreated.addListener(creating)

    chrome.windows.onRemoved.addListener(removeId => {
      if (id === removeId) {
        chrome.tabs.onAttached.removeListener(attaching)
        chrome.tabs.onCreated.removeListener(creating)
        props.onRemove()
      }
    })
  }

  UNSAFE_componentWillUpdate() {
    console.log('Window ' + this.props.id + ' update')
  }

  render() {
    return <div
      className = {[
        'Window',
        'view-' + this.props.viewStyle
      ].join(' ')}
    >
      {this.getChildren.map(id => 
        <Tab key = {id}
          viewStyle = {this.props.viewStyle}
          id = {id}
          onRemove = {() => this.removeChild(id)}
        />
      )}
    </div>
  }
}
