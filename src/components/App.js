/*global chrome*/
import React from 'react'
import PureChildrenedComponent from './basic/PureChildrenedComponent'
import Window from './Window'

const viewStyles = ['details', 'mosaic']

export default class App extends PureChildrenedComponent {
  constructor(props) {
    const mySuper = super(props)
    this.state = Object.assign(mySuper.state,
      {viewStyleInd: 0}
    )

    chrome.windows.getAll(ws => 
      this.setChildren(ws.map(w => w.id))
    )
    chrome.windows.onCreated.addListener(({id}) =>
      this.addChild(id)
    )
  }

  UNSAFE_componentWillUpdate() {
    console.log('App update')
  }
  
  render() {
    const viewStyle = viewStyles[this.state.viewStyleInd]
    return Array.prototype.concat(
      <button key = "viewButton"
        onClick = {
          () => this.setState({
            viewStyleInd : (this.state.viewStyleInd + 1) % viewStyles.length
          })
        }
      >
        {viewStyle}
      </button>,
      this.getChildren.map(id => 
        <Window key = {id}
          viewStyle = {viewStyle}
          id = {id}
          onRemove = {() => this.removeChild(id)}
        />
      )
    )
  }
}
