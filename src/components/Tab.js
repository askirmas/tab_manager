/*global chrome*/
import React, {PureComponent} from 'react'
import './Tab.css'

const tabFields = ['index', 'openerTabId', 'active', 'pinned', 'url', 'title', 'favIconUrl', 'status'];

export default class Tab extends PureComponent {
  constructor(props) {
    super(props)
    const {id} = props
    chrome.tabs.get(id, tab => {
      this.setState(tabFields.reduce(
        (acc, field) => {
          acc[field] = tab[field]
          return acc
        },
        {}
      ))    
    })
    
    const destroying = removedId => {
      if (removedId === id) props.onRemove()
    }
    chrome.tabs.onDetached.addListener(destroying)
    chrome.tabs.onRemoved.addListener(destroying)
  }

  state = {}

  UNSAFE_componentWillUpdate() {
    console.log('Tab ' + this.props.id + ' update')
  }

  render() {
    const viewStyle = 'view-' + this.props.viewStyle;
    return <div className = {["Tab", viewStyle].join(' ')}>
      <div
        className = {[
          'tab_favIcon',
          viewStyle
        ].join(' ')}
        style = {{backgroundImage: 'url(' + this.state.favIconUrl + ')'}}
      >
        <div className = {[
          this.state.status !== 'complete' ? [] : 'invisible',
          'spinner'
        ].join(' ')}/>
      </div>
      <div
        className = {[
          'tab_text',
          viewStyle
        ].join(' ')} 
      >
        <p>{this.state.url}</p>
        <p>{this.state.title}</p>
      </div> 
    </div>
  }
}
