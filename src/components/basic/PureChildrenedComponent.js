import React, {PureComponent} from 'react'

export default class PureChildrenedComponent extends PureComponent {
  getInitialState() {
    console.log('getInitialState');
    return {children : []}
  }
  state = {children : []}
  setChildren = (children) => this.setState({children})
  get getChildren() { 
    return this.state.children
  }
  removeChild = (remove) => this.setState({
    children: this.state.children.filter(id => id !== remove)
  })
  addChild = (add) => this.setState({
    children: this.state.children.concat([add])
  })
}
